#include <stdio.h>

struct fraction
    {int num;int den;};
struct fraction input()
    {struct  fraction f;
     scanf("%d/%d",&f.num,&f.den);
     return f;
        }
struct fraction compute(struct fraction f1,struct fraction f2)
    {f1.num=f1.num*f2.den+f2.num*f1.den;
     f1.den*=f2.den;
     return f1;
        }
struct fraction simplify(struct fraction sum)
    {int lim,l,factor;
     lim=0;
     factor=1;
     if(sum.num>sum.den||sum.num==sum.den)lim=sum.den;
     else lim=sum.num;
     for(l=1;l<=lim;l++)
     {if(sum.num%l==0&&sum.den%l==0)factor=l;
      else continue;}
     sum.num/=factor;
     sum.den/=factor;
     return sum;
        }
void output(struct fraction sum)
    {printf("The sum is %d/%d",sum.num,sum.den);
        }
void main()
    {struct fraction f2,sum,simpl;int i,n;
     printf("Enter first fraction\n");
     sum=input();
     for(i=2;i>=1;i++)
         { printf("\nEnter fraction to be added\n");
           f2=input();
           sum=compute(sum,f2);
           printf("Enter 1 to continue and 0 to stop adding");
           scanf("%d",&n);
           if(n==1)continue;else break;}
     simpl=simplify(sum);
     output(simpl);
        }